import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <>
      <nav className="navbar is-link is-fixed-top" role="navigation" aria-label="main navigation">
        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <p>chitty chitty chat</p>
            <NavLink className="navbar-item" to="/room/1">room 1</NavLink>
            <NavLink className="navbar-item" to="/room/2">room 2</NavLink>
            <NavLink className="navbar-item" to="/room/3">room 3</NavLink>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Nav;
