import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import ChatRoom from './ChatRoom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <BrowserRouter>
        <Nav />
        <Routes>
          <Route path="/room/:id" element={<ChatRoom />} />
        </Routes>
      </BrowserRouter>
  </React.StrictMode>
);
