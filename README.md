# FastAPI, MongoDB, and React

This is an example application that shows how to build a Web
application with FastAPI, MongoDB, and React.

## Running the application

1. Clone this repo.
1. Change your working directory to the cloned repo's
   directory.
1. Run the following commands:

    ```sh
    docker volume create example-mongo-data
    docker compose build
    docker compose up
    ```

## View OpenAPI documentation

Access the docs at:

* API: <http://localhost:8001/docs>

## Logging in

Access the GHI at <http://localhost:3000>.

## Playing with WebSockets

While you have the application open in a normal browser
window, open another browser window in Incognito Mode.
Access the GHI at <http://localhost:3000>.

In the normal window, set user name to something. In the incognito window set it to something else. Now send messages to each other!!

## Seeing the data

You can see the data in MongoDB in the `messages` database.
You should be able to access the documents at
<http://localhost:8081>.
