import os
from fastapi import FastAPI, WebSocket, WebSocketDisconnect, Depends
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import json

from client import Queries


app = FastAPI()

origins = [
    os.environ.get("CORS_HOST", "http://localhost:3000"),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

class MessageIn(BaseModel):
    username: str
    room_id: int
    input: str

class MessageOut(MessageIn):
    id: str

def sanitize_message(msg: str) -> MessageOut:
    msg["id"] = str(msg["_id"])
    del msg["_id"]
    return MessageOut(**msg)

class MessageQueries(Queries):
    DB_NAME = "chittychat"
    COLLECTION = "messages"

    def create(self, message) -> MessageOut | None:
        res = self.collection.insert_one(message)
        if res.inserted_id:
            return self.get_message_by_id(res.inserted_id)

    def get_message_by_id(self, id) -> MessageOut | None:
        result = self.collection.find_one({ "_id": id })
        if result:
            return sanitize_message(result)

    def get_messages_by_room_id(self, room_id) -> list[MessageOut] | None:
        results = list(self.collection.find({ "room_id": room_id }))
        results = list(map(sanitize_message, results))
        if results:
            return results

connections = {}


@app.websocket("/chatroom/{room_id}/{username}")
async def websocket_endpoint(
    room_id: str,
    username: str,
    websocket: WebSocket,
    queries: MessageQueries = Depends(),
):
    await websocket.accept()
    connections[username] = { "connection": websocket, "room_id": room_id }
    try:
        while True:
            data = await websocket.receive_text()
            message = MessageIn(json.loads(data))
            new_msg = queries.create(message)
            for connection in connections.values():
                if connection['room_id'] == room_id:
                    await connection['connection'].send_text(json.dumps(new_msg))
    except WebSocketDisconnect:
        del connections[username]


@app.get("/api/messages/{room_id}", response_model=list[MessageOut] | None)
async def get_messages_by_room_id(
    room_id: str,
    queries: MessageQueries = Depends(),
):
    return queries.get_messages_by_room_id(room_id)
